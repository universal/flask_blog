# flask-blog
Flask Blog flask做的简单博客

###说明
Flask 版本 0.10.1
###需要安装的扩展
#### Flask-SQLAlchemy
```
pip install Flask-SQLAlchemy
```
####Flask-PyMongo-0.3.1 PyMongo-2.8
```
pip install Flask-PyMongo
```
####MySQL-python-1.2.5
```
pip install MySQL-python
```
####Flask-Assets
```
pip install Flask-Assets
```
####uwsgi
```
pip install uwsgi
```
